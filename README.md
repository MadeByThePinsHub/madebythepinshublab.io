# Group Site Source
The repository you are viewing is the group site source of the Pins team in GitLab.

## Documentation
The full documentation for managing this repository are always found in the officil handbook. The following table below

| Topic Covered | Documentation Path | URL |
| ------------- | ------------------ | --- |
| Managing Blog Posts | `handbook/life-at-the-pins/mainterance/main-site/blog-posts.md` | <https://en.handbooksbythepins.gq/life-at-the-pins/mainterance/main-site/blog-posts> |
| Community Links | `handbook/life-at-the-pins/mainterance/main-site/community-links.md` | <https://en.handbooksbythepins.gq/life-at-the-pins/mainterance/main-site/community-links>
| Team Members Directory | `handbook/directory/team-members.md` (mirror) | <https://en.handbooksbythepins.gq/directory/team-members> |
