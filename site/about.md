# About the Team

## Meet the Squad

* [Andrei Jiroh](../u/ajhalili2006) - Team Leader and Lead Developer/Maintainer

## Contact Info

Contact infos are currently moving into [our new contact page](/contact.md)

### Contacting the Team in Encrypted Way
If you want to keep third-parties from seeing our conversations, we advised to use secure methods of contacting the team.

* **Telegram**: In the Telegram app, search `@ThePinsTeam`, open its profile and tap an button that creates a new secret chat if you prefer not to use their bulit-in Cloud Chats.
* **Keybase**: Search `thepinsteam` or `thepinsupport`, tap **Follow** then **Chat**. Your deepest secrets are end-to-end encrypted across devices. [Learn more about verifying our identities with Keybase](../handbook/devops/secure/verifying-keys-with-keybase)
* If emailing us, use [our keys](https://signoff.rtapp.tk/misc/signing-emails-with-pgp) when sending through an GPG-compartible email client (Vivaldi Web Mail for Vivaldi users should be fine).

[1]: https://gitlab.com/MadeByThePinsTeam-DevLabs/legal/complaints-hub/issues
