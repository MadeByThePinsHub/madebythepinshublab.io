# The Pins Team
Welcome to the main website of the Pins team! This site would be small because most of the things are on our [handbook](#see-more)

## Directory
* [About the team and Contacts](about)
* [Join the Pins team](join)
* [Read our team blog](blog)

## See more
* New to the team or simply want a look at our internal workings? See [our handbook](https://en.handbooksbythepins.gq), for the full scoop.
* [Join the community and help us shape the future of the team.](community)
