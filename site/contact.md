# Contacting The Pins Team

Full list of our contact informations for different things. Please use the correct channel for the right type of inquiry and we're always looking talking to you soon.

## General Inquiries and Support + Socials

* Email: <yourfriends@madebythepins.tk> (general), <support@recaptime.tk> (support)
* Twitter: [@ThePinsTeam](https://twitter.com/thepinsteam)
* Mastodon: [@thepinsteam@mastodon.online](https://mastodon.online/@thepinsteam)
* Telegram: [@ThePinsTeam](https://telegram.me/ThePinsTeam)

### General Support Rules

* No spam, scam, and cyrptocurrency nonsense bullshit.
* We're not Stack Overflow or DuckDuckGo (or whatever search engine you're using)
* Don't demand speedy response, humans may reply within 48+ hours.
* Please, RTFM first.

## Abuse / Community Code of Conduct Violations Reporting

* Online Form: <https://rtapp.tk/report-abuse> (hosted on Google Forms)
* Email: <abuse@madebythepins.tk>, <codeofconduct@madebythepins.tk>

## Open-source

* GitHub: [@MadeByThePinsHub](https://github.com/MadeByThePinsHub)
* GitLab SaaS: [@MadeByThePinsHub](https://gitlab.com/MadeByThePinsHub)
* Launchpad.net: [~madebythepinshub](https://launchpad.net/~madebythepinshub
