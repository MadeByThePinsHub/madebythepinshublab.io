# Community

## Official Keybase teams
* `madebythepinsteam` - Main community open big team
* `madebythepinsteam.recaptime` - Recap Time

## Official Telegram chats
* [**Main Community Chat**](https://t.me/ThePinsTeamCommunity) - Talk about our team and an place to chill.
* [Recap Time Live Chat](https://t.me/RecapTime_RecapTime)
* [Public Community Support](https://t.me/MPSupportChat)
* [**Off-topic Discussion**](https://t.me/ShitpostingCentralChat) - Test non-spammy userbot commands and do chit-chat there.
* [Developers' Chat](https://t.me/CodersChat_byMPTeam)

## Discord Servers
Will be updated soon...

## Looking for Slack?
!!! warning "Slack is deprecated and under phrase-out process"
    We'll no longer accept any new accounts to our Slack teams and will be archived for historical purposes. Use [Keybase](#official-keybase-teams),
    [Discord](#discord-servers) or [Telegram](#official-telegram-chats) instead.
